package com.ivan;

import com.ivan.entity.User;
import com.ivan.services.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootmybatisplusApplicationTests {
	@Autowired
	private IUserService ius;

	@Test
	public void contextLoads() {
        List<User> list = ius.list(null);
        list.stream().forEach(user -> {
            System.out.println(user);
        });
    }@Test
	public void testUser() {
        User user = ius.getById("1");
        System.out.println(user.toString());
    }

}
