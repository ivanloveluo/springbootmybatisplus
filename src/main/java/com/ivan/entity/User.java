package com.ivan.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxw
 * @since 2019-07-19
 */
@Data
@TableName("t_user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    /**
     * 数据唯一性ID
     */
    @TableId(type = IdType.UUID)
    private String USERID;

    /**
     * 登录名
     */
    private String LOGINNAME;

    /**
     * 旧密码
     */
    private String OLDPWD;

    /**
     * 密码
     */
    private String PASSWORD;

    /**
     * 真实姓名
     */
    private String USERNAME;

    /**
     * 用户类型
     */
    private String USERTYPE;

    /**
     * 电话
     */
    private String TEL;

    /**
     * Email
     */
    private String EMAIL;

    /**
     * 地址
     */
    private String LOCALADDRESS;

    /**
     * 描述
     */
    private String REMARK;

    /**
     * 最后登录时间
     */
    private LocalDateTime LASTLOGINDATE;

    /**
     * 创建时间
     */
    private LocalDateTime CREATETIME;

    /**
     * 附件地址
     */
    private String FJDZ;

    /**
     * 身份证/企业证号
     */
    private String IDCARD;

    /**
     * 创建时间
     */
    private LocalDateTime CREATEDATE;

    /**
     * 创建者
     */
    private String CREATEUSER;

    /**
     * 删除标识 0未删除 1已删除 
     */
    private Integer DELFLAG;

    @Override
    protected Serializable pkVal() {
        return this.USERID;
    }
}
