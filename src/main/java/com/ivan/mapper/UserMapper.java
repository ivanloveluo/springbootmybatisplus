package com.ivan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ivan.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxw
 * @since 2019-07-19
 */
public interface UserMapper extends BaseMapper<User> {

}
