package com.ivan.services.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ivan.entity.User;
import com.ivan.mapper.UserMapper;
import com.ivan.services.IUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxw
 * @since 2019-07-19
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
