package com.ivan.services;

import com.ivan.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxw
 * @since 2019-07-19
 */
public interface IUserService extends IService<User> {

}
