package com.ivan.utils;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.utils
 * @Description: TODO
 * @date 2019/7/19 11:14
 * 代码生成器
 */
public class MpGenerator {
     public static void main(String[] args) throws Exception {
         AutoGenerator mpg = new AutoGenerator();
         // 选择 freemarker 引擎，默认 Velocity
         mpg.setTemplateEngine(new FreemarkerTemplateEngine());
         // 全局配置
         GlobalConfig gc = new GlobalConfig();
         gc.setAuthor("lxw");
         gc.setOutputDir("d://model");
         gc.setFileOverride(false);
         gc.setFileOverride(true);// 是否覆盖同名文件，默认是false
         gc.setActiveRecord(true);// 不需要ActiveRecord特性的请改为false
         gc.setEnableCache(false);// XML 二级缓存
         gc.setBaseResultMap(true);// XML ResultMap
         gc.setBaseColumnList(false);// XML columList

         mpg.setGlobalConfig(gc);
         //配置数据源
         DataSourceConfig dsc = new DataSourceConfig();
         dsc.setDbType(DbType.MYSQL);
         //dsc.setTypeConvert(new MySqlTypeConvert());
         dsc.setDriverName("com.mysql.jdbc.Driver");
         dsc.setUsername("root");
         dsc.setPassword("root");
         dsc.setUrl("jdbc:mysql://127.0.0.1:3306/waepm?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&serverTimezone=UTC");
         mpg.setDataSource(dsc);

         // 策略配置
         StrategyConfig strategy = new StrategyConfig();
         // strategy.setCapitalMode(true);// 全局大写命名 ORACLE 注意
         strategy.setTablePrefix(new String[] { "t_" });// 此处可以修改为您的表前缀
         strategy.setNaming(NamingStrategy.nochange);// 表名生成策略
         strategy.setInclude(new String[] { "t_user" }); // 需要生成的表
         // strategy.setExclude(new String[]{"test"}); // 排除生成的表
         mpg.setStrategy(strategy);

         // 包配置
         PackageConfig pc = new PackageConfig();
         pc.setParent("com.ivan");
         // pc.setModuleName("test");
         mpg.setPackageInfo(pc);

         // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】
         // InjectionConfig cfg = new InjectionConfig() {
         // @Override
         // public void initMap() {
         // Map<String, Object> map = new HashMap<String, Object>();
         // map.put("abc", this.getConfig().getGlobalConfig().getAuthor() +
         // "-mp");
         // this.setMap(map);
         // }
         // };
         //
         // // 自定义 xxList.jsp 生成
         // List<FileOutConfig> focList = new ArrayList<>();
         // focList.add(new FileOutConfig("/template/list.jsp.vm") {
         // @Override
         // public String outputFile(TableInfo tableInfo) {
         // // 自定义输入文件名称
         // return "D://my_" + tableInfo.getEntityName() + ".jsp";
         // }
         // });
         // cfg.setFileOutConfigList(focList);
         // mpg.setCfg(cfg);
         //
         // // 调整 xml 生成目录演示
         // focList.add(new FileOutConfig("/templates/mapper.xml.vm") {
         // @Override
         // public String outputFile(TableInfo tableInfo) {
         // return "/develop/code/xml/" + tableInfo.getEntityName() + ".xml";
         // }
         // });
         // cfg.setFileOutConfigList(focList);
         // mpg.setCfg(cfg);
         //
         // // 关闭默认 xml 生成，调整生成 至 根目录
         // TemplateConfig tc = new TemplateConfig();
         // tc.setXml(null);
         // mpg.setTemplate(tc);

         // 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
         // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
         // TemplateConfig tc = new TemplateConfig();
         // tc.setController("...");
         // tc.setEntity("...");
         // tc.setMapper("...");
         // tc.setXml("...");
         // tc.setService("...");
         // tc.setServiceImpl("...");
         // 如上任何一个模块如果设置 空 OR Null 将不生成该模块。
         // mpg.setTemplate(tc);

         // 执行生成
         mpg.execute();

     }
}
